package be.hics.sandbox.snail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SnailApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnailApplication.class, args);

        int[][] a1 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        prettyPrintSnail(Snail.snail(a1));

    }

    private static void prettyPrintSnail(final int[] snail) {
        for (int i = 0; i < snail.length; i++)
            System.out.println(snail[i]);
    }
}
